import React from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import '../css/pdf.css';
import pdfjsWorker from "pdfjs-dist/build/pdf.worker.entry";

pdfjs.GlobalWorkerOptions.workerSrc = pdfjsWorker;
export default class PdfViewer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            pageNumber: 1,
            pdfUrl: { url: ''},
            url: props.url?props.url:null,
            method: props.method?props.method:'GET',
            printData: props.printData?props.printData:{}
        };
    }

    async componentDidMount(){
        console.assert(this.state.url, "PFD`s url is null");       
        if(this.state.url){
            let self = this;
            let printListUrl = this.state.url;
            let xhr = new XMLHttpRequest();
            if(this.state.method == 'POST'){
                xhr.open('POST', printListUrl, true);
            }else{
                xhr.open('GET', printListUrl, true);
            }
            let authorization = sessionStorage.getItem('Authorization');
            const env = sessionStorage.getItem('x-ebao-env');
            const tenant = sessionStorage.getItem('x-ebao-response-tenant-code');
            xhr.setRequestHeader('x-ebao-env',env);
            xhr.setRequestHeader('x-ebao-tenant-code',tenant);
            xhr.setRequestHeader('Content-Type','application/json');
            xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
            xhr.responseType = 'blob';
            if(this.state.method == 'POST'){
                xhr.send(JSON.stringify(this.state.printData));
            }else{
                xhr.send(null);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    let blob = new Blob([this.response], { type: this.getResponseHeader('content-type') });
                    const r = new FileReader(); // 读取file对象，提取图片base64编码，用于预览
                    r.readAsDataURL(blob);
                    r.onload = e => {
                        const eurl = e.target.result||{};
                        self.setState({pdfUrl: { url: eurl}});
                    };
                }
            };
        }
    }


    onDocumentLoadSuccess = ({ numPages }) => {
        this.setState({ numPages });
    }
  
    render() {
        const { pageNumber, numPages } = this.state;
        const pagination = this.renderPagination(pageNumber, numPages);
        return (
            <div id="rainbow_pdfView">
                <Document
                    file={this.state.pdfUrl}
                    onLoadSuccess={this.onDocumentLoadSuccess}
                >
                    <Page pageNumber={pageNumber} />
                </Document>
                {pagination}
            </div>
        );
    }

    renderPagination = (page, pages) => {
        let previousButton = <span className="previous" onClick={this.handlePrevious}><i className="rainbow SingArrowDown"></i> Previous</span>;
        if (page === 1) {
            previousButton = <span className="previous disabled"><i className="rainbow SingArrowDown"></i> Previous</span>;
        }
        let nextButton = <span className="next" onClick={this.handleNext}>Next <i className="rainbow SingArrowDown"></i></span>;
        if (page === pages) {
            nextButton = <span className="next disabled">Next <i className="rainbow SingArrowDown"></i></span>;
        }
        return (
            <nav>
                <div id="pager">
                    {previousButton}
                    <span className="page_info">
                        <input type="number" className="page_input" placeholder="page" value={this.state.pageNumber} onChange={this.pageChange.bind(this)}/>
                        <span>of</span>
                        <span>{pages}</span>
                    </span>
                    {/* <span>{page} of {pages}</span> */}
                    {nextButton}
                </div>
            </nav>
        );
    }
    
    handlePrevious = () => {
        this.setState({ pageNumber: this.state.pageNumber - 1 });
    }
    
    handleNext = () => {
        this.setState({ pageNumber: this.state.pageNumber + 1 });
    }

    pageChange(e){
        this.setState({ pageNumber: parseInt(e.target.value)});
    }
    
}
